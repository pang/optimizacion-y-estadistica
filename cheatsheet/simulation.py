import random

# a random experiment
def one_dice():
    return random.randint(1,6)

# a "filter" decides if an outcome belongs to an event
def five_or_more(w):
    return w>=5
def is_even(w):
    return w%2==0

#sample size
N = 10000
#independent random sample
sample = [one_dice() for _ in range(N)]

#approximate the probability that dice is 5 or 6
probA = sum(1 for w in sample if five_or_more(w))/N
#approximate the probability that dice is even
probB = sum(1 for w in sample if is_even(w))/N
#approximate the probability of the intersection
probAB = sum(1 for w in sample
             if five_or_more(w) and is_even(w) )/N

#approximate the conditional probability
probA_cond_B = probAB/probB

#check the two events are independent (approximately)
print(probAB, probA*probB)

#approximate the expectated value
mean = sum(w for w in sample)/N
#approximate the variance
var = sum((w-mean)**2 for w in sample)/N
