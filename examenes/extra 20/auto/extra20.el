(TeX-add-style-hook
 "extra20"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("color" "usenames") ("inputenc" "utf8")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "epsfig"
    "amssymb"
    "amsmath"
    "color"
    "amsfonts"
    "graphicx"
    "inputenc"
    "a4wide"
    "enumitem")
   (TeX-add-symbols
    '("U" 1))
   (LaTeX-add-environments
    "lemma"
    "theorem"
    "thm"
    "proposition"
    "corollary"))
 :latex)

