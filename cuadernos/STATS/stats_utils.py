import random
import numpy as np
import scipy as sc
import scipy.stats as st

import pandas as pd

import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

import statsmodels.formula.api as smf


def plot_empirical_distribution(muestra, dist=None):
    mean, std = dist.mean(), dist.std()
    minx = min(min(muestra)-1, mean-3*std)
    maxx = max(max(muestra)+1, mean+3*std)
    xs = np.concatenate([[minx], sorted(muestra), [maxx]])
    pasoy = 1/len(muestra)
    ys = np.concatenate( [np.arange(0,1,pasoy), [1,1]])
    plt.step(xs, ys, where='post', label='cdf empírica')
    if dist:
        xs = np.arange(minx,maxx,std*0.01)
        ys = dist.cdf(xs)
        plt.plot(xs,ys, label = 'cdf')
    plt.legend()
    
canonical_percentiles = [
    1-2*st.norm(loc=0,scale=1).cdf(-k) for k in (1,2,3)
]

def plotMN(MN, sample_size=1000, npoints=100):
    #Muestra aleatoria de la normal multivariable
    means, Sigma = MN.mean, MN.cov
    # Create grid coordinates for plotting
    std1, std2 = np.sqrt([Sigma[0,0], Sigma[1,1]])
    X1 = np.linspace(means[0] - 4*std1, means[0] + 4*std1, npoints)
    X2 = np.linspace(means[1] - 4*std2, means[1] + 4*std2, npoints)
    xx, yy = np.meshgrid(X1,X2, indexing='xy')
    Z = np.zeros((X1.size,X2.size))

    for i,x1i in enumerate(X1):
        for j, x2j in enumerate(X2):
            Z[j,i] = MN.pdf([x1i,x2j])

    #Queremos dibujar exactamente los contornos que capturan 
    # los percentiles habituales (~68%,95%,99%):
    #https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Interval
    factor = 1/(2*np.pi*np.sqrt(np.linalg.det(Sigma)))
    E = st.expon(scale=2).ppf
    levels = [
        factor*np.exp(-0.5*E(p))
        for p in reversed(canonical_percentiles)
    ]

    plt.contour(xx, yy, Z, cmap=plt.cm.Set2_r, levels=levels)
    plt.contourf(xx, yy, Z, cmap=plt.cm.Set2_r, alpha=0.4, levels=levels)
    
def plot_dists(dists, xmin = None, xmax=None):
    if  (xmin is None) or (xmax is None):
        xmin = min(dist.ppf(0.01) for dist in dists)
        xmax = max(dist.ppf(0.99) for dist in dists)
        L = (xmax - xmin)
        xmin, xmax = xmin - 0.1*L, xmax + 0.1*L
    xs = np.linspace(xmin, xmax,300)
    plt.figure(figsize=(16,12))
    for dist in dists:
        ys = dist.pdf(xs)
        lines = plt.plot(
            xs, ys,
            label='%s, $\mu=%.2f, \sigma=%.2f$'%(
                dist.kwds, dist.mean(), dist.std()), 
            lw=2)
        plt.fill_between(xs, 0, ys, alpha = 0.2, color = lines[0].get_color())
        plt.autoscale(tight=True)
    plt.ylim(0)
    plt.legend(title="parameters")
    

# Regresión no lineal
def plot_2D_linear_model_as_surface(df, colx, coly, colz):
    #Regresión colZ ~ colX + colY
    # colZ = b0 + bX*colX + bY*colY
    formula = '%s ~ %s + %s'%(colz, colx, coly)
    est = smf.ols(formula, df).fit()

    dfc = df.copy()
    #Añadimos una columna con los residuos (con signo)
    dfc['res'] = est.resid
    # Create a coordinate grid
    minx, maxx = min(df[colx]), max(df[colx])
    miny, maxy = min(df[coly]), max(df[coly])
    Lx, Ly = maxx - minx, maxy - miny
    
    X = np.linspace(minx - 0.2*Lx, maxx + 0.2*Lx, 100)
    Y = np.linspace(miny - 0.2*Ly, maxy + 0.2*Ly, 100)

    BX, BY = np.meshgrid(X,Y, indexing='xy')
    Z = np.zeros((X.size, Y.size))

    intercept, beta_X, beta_Y = est.params
    for (i,j),v in np.ndenumerate(Z):
        Z[i,j] =(intercept + BX[i,j]*beta_X + BY[i,j]*beta_Y)
            
    # Create plot
    fig = plt.figure(figsize=(6,6))
    fig.suptitle('Regression: %s'%formula, fontsize=20)

    ax = axes3d.Axes3D(fig)

    ax.plot_surface(BX, BY, Z, rstride=10, cstride=5, alpha=0.4)
    dfc_plus = dfc[dfc.res>0]
    dfc_minus = dfc[dfc.res<=0]
    ax.scatter3D(dfc_plus[colx], dfc_plus[coly], dfc_plus[colz], c='r')
    ax.scatter3D(dfc_minus[colx], dfc_minus[coly], dfc_minus[colz], c='y')

    ax.set_xlabel(colx)
    ax.set_xlim(minx - 0.2*Lx, maxx + 0.2*Lx)    
    ax.set_ylabel(coly)
    ax.set_ylim(miny - 0.2*Ly, maxy + 0.2*Ly)
    ax.set_zlabel(colz)

def plot_poly_reg_fit_predict(X, y, x_new):
    Lx = X.max().values - X.min().values
    xmin, xmax = X.min().values - Lx/10, X.max().values + Lx/10
    xmax = max(xmax, x_new+Lx/10)


    plt.figure(figsize=(8,8))
    plt.scatter(X, y, facecolors='r', edgecolors='k', alpha=0.6, s=20)
    colors = 'rgbcmyk'
    y_predictions = []
    for order in range(1,6):
        poly = PolynomialFeatures(order)
        Xpoly = poly.fit_transform(X)

        regr = skl_lm.LinearRegression()

        regr.fit(Xpoly,y)

        y_new = regr.predict(poly.fit_transform([[x_new]]))
        y_predictions.append(y_new)

        #xs son puntos equiespaciados entre xmin y xmax
        xs = np.arange(xmin, xmax, Lx/100)
        #ys es el resultado de aplicar el modelo a cada punto de xs
        ys = regr.predict(poly.fit_transform(xs.reshape(-1,1)))
        #asi que plot(xs,ys) dibuja la grafica del modelo mpg = f(hp)
        plt.plot(xs, ys, 
                 label='order %d'%order, 
                 color=colors[order%(len(colors))])
        #Resaltamos la prediccion para hp=250
        plt.scatter([x_new], regr.predict(poly.fit_transform([[x_new]])), 
                    facecolors=colors[order%(len(colors))],
                    edgecolors='k', alpha=0.6, s=20)
        plt.legend()
    plt.ylim(0,max(y.max(), max(y_predictions))*1.1)
    plt.xlim(xmin,xmax)