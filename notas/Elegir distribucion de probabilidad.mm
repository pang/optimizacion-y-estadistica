<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#xbf;Qu&#xe9; distribuci&#xf3;n de probabilidad puede ser un buen modelo para la cantidad o cantidades que no conozco?" FOLDED="false" ID="ID_464828119" CREATED="1619864025456" MODIFIED="1619867652419" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="1.1">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false" show_icon_for_attributes="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="11" RULE="ON_BRANCH_CREATION"/>
<node TEXT="La cantidad que no conozco es una variable aleatoria (un fen&#xf3;meno que se repite, cada vez es independiente de las anteriores)" POSITION="right" ID="ID_1460402167" CREATED="1619864903574" MODIFIED="1619864946146">
<edge COLOR="#ff00ff"/>
<node TEXT="La cantidad observada es un n&#xfa;mero en una lista finita" ID="ID_1050264372" CREATED="1619865656673" MODIFIED="1619865684006">
<node TEXT="" ID="ID_1900827384" CREATED="1619865753351" MODIFIED="1619865753352">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Todos los valores son igual de probables" ID="ID_134186786" CREATED="1619865717391" MODIFIED="1619865747627"/>
<node TEXT="" ID="ID_1127721564" CREATED="1619865753349" MODIFIED="1619865753351">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Uniforme Discreta" ID="ID_555757553" CREATED="1619865753352" MODIFIED="1619865766480">
<node TEXT="N: la VA toma valores 1...N" ID="ID_1552399890" CREATED="1619865772024" MODIFIED="1619865789854"/>
</node>
</node>
<node TEXT="" ID="ID_893843254" CREATED="1619866363440" MODIFIED="1619866363443">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="S&#xf3;lo puede tomar dos valores, que codificamos con 1 o 0." ID="ID_785114644" CREATED="1619866266956" MODIFIED="1619866332424"/>
<node TEXT="" ID="ID_1381553481" CREATED="1619866363432" MODIFIED="1619866363438">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Bernouilli" ID="ID_1213217190" CREATED="1619866363444" MODIFIED="1619866367688">
<node TEXT="p:probabilidad de obtener 1" ID="ID_1913413085" CREATED="1619866378240" MODIFIED="1619866387125"/>
</node>
</node>
<node TEXT="" ID="ID_357198809" CREATED="1619865879337" MODIFIED="1619865879339">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Contamos el total de &quot;positivos&quot; en una poblaci&#xf3;n, todos los individuos tienen la misma probabilidad de positivo y son independientes" ID="ID_1893364424" CREATED="1619865805494" MODIFIED="1619865870225"/>
<node TEXT="" ID="ID_1548446263" CREATED="1619865879335" MODIFIED="1619865879336">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Binomial" ID="ID_1074003959" CREATED="1619865879339" MODIFIED="1619865883517">
<node TEXT="p: probabilidad de positivo" ID="ID_1065805193" CREATED="1619865884844" MODIFIED="1619865887949"/>
<node TEXT="N: tama&#xf1;o de la poblaci&#xf3;n" ID="ID_1275640931" CREATED="1619865888438" MODIFIED="1619865898719"/>
</node>
</node>
</node>
<node TEXT="La cantidad observada es un n&#xfa;mero entero positivo" ID="ID_144872056" CREATED="1619865055868" MODIFIED="1619865071721">
<node TEXT="" ID="ID_539125495" CREATED="1619866010410" MODIFIED="1619866010414">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Es el n&#xfa;mero de intentos hasta el primer &quot;&#xe9;xito&quot;" ID="ID_473677956" CREATED="1619865080679" MODIFIED="1619865134818"/>
<node TEXT="Es el n&#xfa;mero de individuos hasta encontrar al primero con cierta propiedad" ID="ID_60762544" CREATED="1619865118551" MODIFIED="1619865160788"/>
<node TEXT="" ID="ID_802526409" CREATED="1619866010403" MODIFIED="1619866010408">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Geom&#xe9;trica" ID="ID_1240021258" CREATED="1619866010417" MODIFIED="1619866014779">
<node TEXT="p: probabilidad de &#xe9;xito en un &#xfa;nico intento" ID="ID_185519680" CREATED="1619866016335" MODIFIED="1619866028387"/>
</node>
</node>
<node TEXT="" ID="ID_1343917320" CREATED="1619866042698" MODIFIED="1619866042700">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Es el n&#xfa;mero de observaciones de un fen&#xf3;meno raro en un intervalo" ID="ID_159586666" CREATED="1619865967254" MODIFIED="1619865987444"/>
<node TEXT="" ID="ID_1458496434" CREATED="1619866042695" MODIFIED="1619866042697">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Poisson" ID="ID_813630302" CREATED="1619866042701" MODIFIED="1619866047927">
<node TEXT="mu: media de observaciones en el intervalo: t&#xed;picamente es el producto de la tasa de observaciones por la longitud del intervalo de observaci&#xf3;n" ID="ID_1938346656" CREATED="1619866051403" MODIFIED="1619866054653"/>
</node>
</node>
</node>
<node TEXT="La cantidad observada es un n&#xfa;mero real positivo" ID="ID_1543816867" CREATED="1619864377372" MODIFIED="1619864979803">
<node TEXT="" ID="ID_864518883" CREATED="1619864792407" MODIFIED="1619864792409">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Es el tiempo entre dos observaciones de un fen&#xf3;meno raro" ID="ID_1846644497" CREATED="1619864396419" MODIFIED="1619864601907"/>
<node TEXT="Es el tiempo desde que comienzo a observar hasta que observo el fen&#xf3;meno" ID="ID_1167739445" CREATED="1619864433073" MODIFIED="1619864643585"/>
<node TEXT="Es otra medida entre dos observaciones de ese fen&#xf3;meno raro" ID="ID_285093528" CREATED="1619864644286" MODIFIED="1619864677868">
<node TEXT="N&#xfa;mero de p&#xe1;ginas hasta la primera errata" ID="ID_355706461" CREATED="1619864680492" MODIFIED="1619864690239"/>
<node TEXT="N&#xfa;mero de metros de tuber&#xed;a hasta el siguiente defecto" ID="ID_1207062952" CREATED="1619864698484" MODIFIED="1619864710701"/>
<node TEXT="N&#xfa;mero de transmisiones hasta el siguiente fallo de transmisi&#xf3;n" ID="ID_508516539" CREATED="1619864731686" MODIFIED="1619864746146"/>
<node TEXT="etc&#xe9;tera" ID="ID_887388914" CREATED="1619864750949" MODIFIED="1619864753097"/>
</node>
<node TEXT="" ID="ID_1369737483" CREATED="1619864792404" MODIFIED="1619864792407">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Exponencial" ID="ID_1224767933" CREATED="1619864792411" MODIFIED="1619867996644">
<node TEXT="landa: tasa de eventos por unidad de tiempo, por unidad de longitud, etc" ID="ID_230014934" CREATED="1619865905738" MODIFIED="1619866081067"/>
</node>
</node>
<node TEXT="" ID="ID_903531452" CREATED="1619868414915" MODIFIED="1619868414916">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Velocidad del viento" ID="ID_1179347549" CREATED="1619868266769" MODIFIED="1619868343607"/>
<node TEXT="" ID="ID_1969849569" CREATED="1619868414910" MODIFIED="1619868414914">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Raleigh" ID="ID_1636818587" CREATED="1619868414916" MODIFIED="1619868417177"/>
</node>
<node TEXT="" ID="ID_569182721" CREATED="1619868421206" MODIFIED="1619868421208">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Altura media del oleaje" ID="ID_52533854" CREATED="1619868373325" MODIFIED="1619868400097"/>
<node TEXT="Es el tiempo hasta el fallo, pero de un componente que sufre envejecimiento" ID="ID_672530242" CREATED="1619868232536" MODIFIED="1619868264913"/>
<node TEXT="" ID="ID_1858634705" CREATED="1619868421204" MODIFIED="1619868421206">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Weibull" ID="ID_1960750679" CREATED="1619868421209" MODIFIED="1619868423583"/>
</node>
</node>
<node TEXT="" ID="ID_877329918" CREATED="1619866233078" MODIFIED="1619866233079">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="La cantidad observada es una suma de muchas contribuciones independientes" ID="ID_90061516" CREATED="1619866089485" MODIFIED="1619866224572"/>
<node TEXT="" ID="ID_1571546509" CREATED="1619866233078" MODIFIED="1619866233078">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Normal" ID="ID_1630370147" CREATED="1619866233080" MODIFIED="1619866235356">
<node TEXT="mu: valor medio" ID="ID_1725410467" CREATED="1619866238925" MODIFIED="1619866246930"/>
<node TEXT="sigma: desviaci&#xf3;n t&#xed;pica" ID="ID_1409028690" CREATED="1619866247502" MODIFIED="1619866254727"/>
</node>
</node>
<node TEXT="" ID="ID_1272210831" CREATED="1619866486459" MODIFIED="1619866486460">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="La cantidad observada siempre est&#xe1; en un intervalo [a,b], todos los subintervalos de [a,b] de la misma longitud son igual de probables" ID="ID_986564539" CREATED="1619866413570" MODIFIED="1619867729045"/>
<node TEXT="" ID="ID_1165036842" CREATED="1619866486458" MODIFIED="1619866486459">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Uniforme Continua" ID="ID_384235699" CREATED="1619866486460" MODIFIED="1619866490556">
<node TEXT="a: extremo inferior del intervalo" ID="ID_947669028" CREATED="1619866496692" MODIFIED="1619866505287"/>
<node TEXT="b: extremo superior del intervalo" ID="ID_323803801" CREATED="1619866505744" MODIFIED="1619866513391"/>
</node>
</node>
</node>
<node TEXT="Lo que no conozco es un par&#xe1;metro de una distribuci&#xf3;n (Inferencia Param&#xe9;trica Bayesiana)" POSITION="left" ID="ID_1652742335" CREATED="1619864949235" MODIFIED="1619865016867">
<edge COLOR="#00ffff"/>
<node TEXT="" ID="ID_565839609" CREATED="1619866546754" MODIFIED="1619866546755">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="No conozco la tasa de un proceso de Poisson" ID="ID_1721863014" CREATED="1619866529388" MODIFIED="1619866540667"/>
<node TEXT="" ID="ID_1910964450" CREATED="1619866546752" MODIFIED="1619866546753">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Gamma" ID="ID_161876118" CREATED="1619866546755" MODIFIED="1619866548951">
<node TEXT="Gamma(1,0) es el prior uniforme" ID="ID_532568703" CREATED="1619866575065" MODIFIED="1619866588113"/>
<node TEXT="Gamma(1/2,0) es el prior de Jeffreys" ID="ID_212254490" CREATED="1619866597175" MODIFIED="1619866618601"/>
<node TEXT="Si mi prior es Gamma(a,b) y observo x eventos en un intervalo de longitud T, el posterior es Gamma(a+x,b+T)" ID="ID_24016331" CREATED="1619866690284" MODIFIED="1619866740634"/>
</node>
</node>
<node TEXT="" ID="ID_539608131" CREATED="1619866760283" MODIFIED="1619866760284">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="No conozco una probabilidad" ID="ID_217805823" CREATED="1619866748974" MODIFIED="1619866755087"/>
<node TEXT="" ID="ID_545704414" CREATED="1619866760281" MODIFIED="1619866760283">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Beta" ID="ID_185485941" CREATED="1619866760284" MODIFIED="1619866762770">
<node TEXT="Beta(1,1) es el prior uniforme" ID="ID_209124790" CREATED="1619866766843" MODIFIED="1619866777440"/>
<node TEXT="Beta(1/2,1/2) es el prior de Jeffereys" ID="ID_330821740" CREATED="1619866781756" MODIFIED="1619866792878"/>
<node TEXT="Si mi prior es Beta(a,b), y observo e &#xe9;xitos y f fracasos, mi posterior es Beta(a+e, b+f)" ID="ID_643286779" CREATED="1619866793644" MODIFIED="1619866840669">
<font BOLD="false"/>
</node>
</node>
</node>
<node TEXT="" ID="ID_887587792" CREATED="1619866886769" MODIFIED="1619866886770">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="No conozco la media una distribuci&#xf3;n Normal" ID="ID_175063024" CREATED="1619866847746" MODIFIED="1619867581353"/>
<node TEXT="" ID="ID_1288832070" CREATED="1619866886768" MODIFIED="1619866886769">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Normal" ID="ID_248212432" CREATED="1619866886771" MODIFIED="1619866889212"/>
</node>
<node TEXT="" ID="ID_512032836" CREATED="1619867070433" MODIFIED="1619867070434">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="No conozco la precisi&#xf3;n (o varianza) de una Normal" ID="ID_1434955051" CREATED="1619867052737" MODIFIED="1619867068199"/>
<node TEXT="" ID="ID_187707674" CREATED="1619867070433" MODIFIED="1619867070433">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Gamma" ID="ID_1060191001" CREATED="1619867070434" MODIFIED="1619867073485"/>
</node>
<node TEXT="" ID="ID_781173216" CREATED="1619867584997" MODIFIED="1619867584998">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="No conozco ni la media ni la precisi&#xf3;n de una Normal" ID="ID_1047714698" CREATED="1619867559125" MODIFIED="1619867571172"/>
<node TEXT="" ID="ID_237463515" CREATED="1619867584995" MODIFIED="1619867584997">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Normal-Gamma" ID="ID_596726098" CREATED="1619867584998" MODIFIED="1619867589074"/>
</node>
<node TEXT="" ID="ID_689900435" CREATED="1619867617064" MODIFIED="1619867617065">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="No conozco los par&#xe1;metros de una Normal Multivariable" ID="ID_1038993404" CREATED="1619867598706" MODIFIED="1619867614030"/>
<node TEXT="" ID="ID_873910710" CREATED="1619867617062" MODIFIED="1619867617064">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Normal-Wishart" ID="ID_158110317" CREATED="1619867617065" MODIFIED="1619867620911"/>
</node>
</node>
<node TEXT="Voy a medir un vector aleatorio: varios n&#xfa;meros reales" POSITION="right" ID="ID_1643120919" CREATED="1619867106630" MODIFIED="1619867143348">
<edge COLOR="#007c7c"/>
<node TEXT="" ID="ID_445601639" CREATED="1619867741496" MODIFIED="1619867741499">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="El vector est&#xe1; limitado a un subconjunto C de R^n, todos los subconjuntos de C de igual &#xe1;rea (o volumen) son igual de probables" ID="ID_1458098265" CREATED="1619867148124" MODIFIED="1619868533607"/>
<node TEXT="" ID="ID_1918647963" CREATED="1619867741494" MODIFIED="1619867741496">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Uniforme Multivariable" ID="ID_259887779" CREATED="1619867741500" MODIFIED="1619867751866">
<node TEXT="Subconjunto de R^n, normalmente un producto de intervalos" ID="ID_270610182" CREATED="1619867760489" MODIFIED="1619867783483"/>
</node>
</node>
<node TEXT="" ID="ID_1616853745" CREATED="1619867861298" MODIFIED="1619867861301">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Cada componente del vector aleatorio es Normal (y las combinaciones lineales de las componentes del vector tambi&#xe9;n son Normales)" ID="ID_438441485" CREATED="1619867857074" MODIFIED="1619867859412"/>
<node TEXT="" ID="ID_1826605267" CREATED="1619867861297" MODIFIED="1619867861298">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Normal Multivariable" ID="ID_522779576" CREATED="1619867861303" MODIFIED="1619867867600">
<node TEXT="mu: vector con la media de cada componente" ID="ID_1073998380" CREATED="1619867870800" MODIFIED="1619867886033"/>
<node TEXT="Sigma: matriz de varianzas-covarianzas" ID="ID_378590151" CREATED="1619867886505" MODIFIED="1619867901455"/>
</node>
</node>
</node>
</node>
</map>
