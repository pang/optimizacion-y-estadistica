<TeXmacs|1.99.13>

<style|<tuple|generic|spanish>>

<\body>
  <\description>
    <item*|Experimento aleatorio, o (random experiment)>Cualquier fenómeno
    cuyo resultado no se puede predecir (tanto si es por desconocimiento o
    porque es intrínsecamente aleatorio).

    <item*|Resultado (outcome)>Cada observación del experimento termina en un
    resultado, que es aquella cantidad, u objeto que registramos y que
    queremos estudiar.

    <item*|Espacio muestral (Sample space)>El conjunto de todos los posibles
    resultados del experimento.

    <item*|Evento o suceso (event)>

    <item*|Evento elemental (simple event)>

    <item*|Evento compuesto (compound event)>
  </description>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|prog-scripts|python>
  </collection>
</initial>