# Optimización y Estadística

Todo el *código fuente* de la asignatura optativa de **Optimización y Estadística** impartida en la **ETS de Ingenieros Navales de la UPM en Madrid**:

 - temario detallado, temporalización y lecturas recomendadas para cada semana
 - resúmenes de teoría (dos *cheatsheet* en *inglés*)
 - hojas de ejercicios (en *castellano*)
 - exámenes, la mayoría parametrizados (para generar variantes diferentes) y resueltos (en *castellano*)
 - cuadernos `jupyter` con prácticas en `python` (en *castellano*)

La asignatura funciona para docencia presencial, semi-presencial y telemática, pero no es un MOOC y los alumnos necesitan ayuda de un profesor.

## Install / Build

 - Las cheatsheet, hojas de ejercicios y varios de los exámenes están disponibles en `pdf`. Para poder editarlas es suficiente instalar `LaTeX`.
 - Parte de los exámenes fueron generados con [auto-multiple-choice](https://auto-multiple-choice.net/), y es necesario instalar este programa para poder editarlos.
 - Los cuadernos `jupyter` usan `python3` y algunas librerías. Se pueden instalar todas las librerías necesarias con un comando:

```python
python3 -m pip install numpy scipy matplotlib ipython jupyter pandas sympy optlang nose scikit-learn seaborn statsmodels
```

## Temario

... Coming soon ...

## Cheatsheet

Dos chuletas (*cheatsheet*) en inglés:

  - `milp-cheatsheet.pdf`, resumen de comandos `python`, `numpy` y `matplotlib`, y un resumen de programación lineal con variables mixtas (MILP).
  - `prob-cheatsheet.pdf`, basada en [probability cheatsheet de wzchen](https://github.com/wzchen/probability_cheatsheet), pero muy adaptada a este curso, incluyendo una tabla con *operaciones de variables aleatorias*, una tabla de *inferencia paramétrica*, y comandos `python` útiles para estadística.

## Hojas de ejercicios, en *castellano*

  - **MILP**, programación lineal con variables continuas y enteras.
  - **INFER**, problemas de *inferencia paramétrica*: Máxima verosimilitud, distribuciones conjugadas e inferencia paramétrica bayesiana

Las siguientes hojas las heredamos de una asignatura anterior, se trata de una recopilación de ejercicios del profesor [Antonio Souto-Iglesias](http://canal.etsin.upm.es/contacto/).
Algunos de los ejercicios son originales:

  - **PROB1**, probabilidad elemental, teorema de la probabilidad total y teorema de Bayes
  - **PROB2**, VA discretas
  - **PROB3**, VA continuas
  - **PROB4**, distribuciones multidimensionales, sumas de VAs, teorema central del límite.

## Exámenes

  - Tres archivos `zip` contienen ficheros pdf con todos los exámenes de los cursos 17/18, 18/19 y 19/20.
  - Código fuente, que puede ser:
    + Fichero `tex1`
    + Cuaderno jupyter, `ipynb`
    + Parte de los exámenes fueron generados con [auto-multiple-choice](https://auto-multiple-choice.net/). Incluímos los ficheros fuente, a falta del ficheor `students.csv`, en el formato estándar de `auto-multiple-choice`.

## Cuadernos `jupyter`

### Carpeta `MILP`

  - [`Intro_LP`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Intro_LP.ipynb): introducción a la programación lineal con variables continuas
  - [`Intro_MILP`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Intro_MILP.ipynb): introducción a la programación lineal con variables mixtas, continuas y enteras  ("Mixed Integer Linear Programming")
  - [`Dieta`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Dieta.ipynb): aplicación al diseño de la dieta óptima
  - [`Transport`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Transport.ipynb): aplicación al problema de transporte
  - [`knapsack`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/knapsack.ipynb): aplicación al problema de mochila
  - [`sudoku`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/sudoku.ipynb): aplicación a la resolución de Sudokus (problemas similares a confeccionar horarios y planes de trabajo)
  - [`viajante`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/viajante.ipynb): aplicación al problema del viajante ("Travelling salesman")

#### Subcarpeta `puzzles`

Algunos ejemplos de aplicación de MILP a puzzles lógicos.

### Carpeta `PROB`

  - [`simulaciones`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/simulaciones.ipynb): simulación de eventos aleatorios para aproximar probabilidades (método de Monte Carlo)
  - [`probabilidad_condicionada`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/probabilidad_condicionada.ipynb): probabilidad condicionada y el teorema de la probabilidad total.
  - [`bayes`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/bayes.ipynb): teorema de Bayes.
  - [`discretas`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/discretas.ipynb): distribuciones de probabilidad discretas (Uniforme, Bernoulli, Binomial).
  - [`esperanza`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/esperanza.ipynb): esperanza y transformaciones de variables aleatorias discretas, simulación versus cálculo exacto.
  - [`discretas_infinitas`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/discretas_infinitas.ipynb): variables aleatorias discretas con soporte infinito (Poisson, Geométrica).
  - [`graficas`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/graficas.ipynb): cómo hacer en `python` las gráficas más habituales.
  - [`continuas_densidad_y_distribucion`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/continuas_densidad_y_distribucion.ipynb): distribuciones de probabilidad continuas (Uniforme, Normal, Exponencial).
  - [`continuas_esperanza`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/continuas_esperanza.ipynb): esperanza y transformaciones de variables aleatorias continuas.
  - [`distribuciones2D`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/distribuciones2D.ipynb): esperanza y transformaciones de variables aleatorias continuas en dos o tres dimensiones (Uniforme multivariable, Normal multivariable).
  - [`continuas_TCL`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/continuas_TCL.ipynb): sumas de variables aleatorias, Ley de los Grandes Números y Teorema Central del Límite.
  - [`group_testing`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/PROB/group_testing.ipynb): ejemplo off-topic que motiva el uso de '_group testing_' para control de epidemias cuando las pruebas diagnósticas son escasas.

### Carpeta `STATS`

- [`pandas-mini`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/pandas-mini.ipynb): introducción a `pandas` para importar datos y hacer un análisis preliminar para comprobar que hemos cargado los datos correctamente.
- [`exploracion`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/exploracion.ipynb): análisis de datos preliminar, generando gráficas y estadísticos para comprobar nuestras conjeturas y preconcepciones sobre los datos.
- [`maxima_verosimilitud`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/maxima_verosimilitud.ipynb): Introducción a la Estadística paramétrica, definición de estimador de máxima verosimilitud, aplicación a las distribuciones estudiadas.
- [`oleaje`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/oleaje.ipynb): Aplicación al análisis de los datos de una serie de datos de viento y oleaje de una boya de medición.
- [`beta`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/beta.ipynb): Introducción a la inferencia paramétrica bayesiana, aplicación al modelo Beta para inferencia de una probabilidad.
- [`gamma-poisson-exponential`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/gamma-poisson-exponential.ipynb): inferencia paramétrica bayesiana, ahora aplicado a la inferencia de la tasa de un proceso de Poisson.
- [`ejercicios_conjugadas`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/ejercicios_conjugadas.ipynb): ejercicios de inferencia paramétrica.
- [`regresion-simple`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/regresion-simple.ipynb): Introducción a la regresión lineal.
- [`regresion-multiple`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/regresion-multiple.ipynb): Introducción a la regresión lineal múltiple. Selección de modelos. Correlación y Causalidad.
- [`regresion-no-lineal`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/STATS/regresion-no-lineal.ipynb): Introducción a la regresión con regresores no lineales. Conjuntos test y train. Overfitting.

## Licencia

Todo el contenido original se distribuye con licencias:

 - [`GFDL`](http://www.gnu.org/copyleft/fdl.html)
 - [`Creative Commons Attribution-Share Alike 3.0 License`](http://creativecommons.org/licenses/by/3.0/deed.es_ES)
 - [`cc-by-nc`](http://creativecommons.org/licenses/by-nc/3.0/deed.es_ES)
 
Salvo la cheatsheet de probabilidad, que sólo se distribuye con la `cc-by-nc`, por ser derivada de material original con esa licencia.
