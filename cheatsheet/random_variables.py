import random

# Omega is "throw three coins"
def threecoins():
    '''Returns a list with three elements, each one of them
    is either 1 (head) or 0 (tails)'''
    return [random.randint(0,1) for _ in range(3)]

# Two random variables are defined on Omega: X and Y
def X(w):
    'counts total number of heads'
    return sum(w)

def Y(w):
    'True if first toss is the same as the last'
    return w[0]==w[-1]

N = 1000
sample = [threecoins() for _ in random(N)]
# Aproximate E[X*Y]
eXY = sum(X(w)*Y(w) for w in sample)/N
