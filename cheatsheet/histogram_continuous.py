E = st.expon(scale=3)
N = 100
sample = E.rvs(N)
plt.hist(sample, density=1,alpha=0.8)
