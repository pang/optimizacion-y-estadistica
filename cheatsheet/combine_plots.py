E1 = st.expon(scale=1)
E2 = st.expon(scale=2)

xmin, xmax = -1,8
N = 100
xs = np.linspace(xmin, xmax, N)
ys1 = E1.pdf(xs)
plt.plot(xs, ys1, 'g', label='density function of Exponential(1)')

ys2 = E2.pdf(xs)
plt.plot(xs, ys2, 'b-', label='density function of Exponential(2)')

plt.title('Density functions of two Exponential distributions')
plt.xlabel('x')
plt.legend()
plt.show()
